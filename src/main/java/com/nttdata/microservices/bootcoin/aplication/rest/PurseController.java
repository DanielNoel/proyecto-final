package com.nttdata.microservices.bootcoin.aplication.rest;

import com.nttdata.microservices.bootcoin.domain.document.PurseDto;
import com.nttdata.microservices.bootcoin.domain.interfaces.PurseService;
import com.nttdata.microservices.bootcoin.util.ResponseUtil;
import javax.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RequiredArgsConstructor
@RestController
@RequestMapping(path = "/purse")
public class PurseController {

  private final PurseService purseService;

  @GetMapping
  @ResponseStatus(HttpStatus.OK)
  private Flux<PurseDto> findAll() {
    return purseService.findAll();
  }

  @GetMapping("/{id}")
  @ResponseStatus(HttpStatus.OK)
  private Mono<ResponseEntity<PurseDto>> findById(@PathVariable("id") String id) {
    return ResponseUtil.wrapOrNotFound(purseService.findById(id));
  }

  @GetMapping(value = "/document/{document-number}")
  @ResponseStatus(HttpStatus.OK)
  private Mono<ResponseEntity<PurseDto>> findByCardNumber(
      @PathVariable("document-number") String documentNumber) {
    return ResponseUtil.wrapOrNotFound(purseService.findByDocumentNumber(documentNumber));
  }

  @PostMapping
  @ResponseStatus(HttpStatus.CREATED)
  private Mono<ResponseEntity<PurseDto>> create(@Valid @RequestBody PurseDto walletDto) {
    return ResponseUtil.wrapOrNotFound(purseService.create(walletDto));
  }

  @PutMapping("/{id}")
  @ResponseStatus(HttpStatus.OK)
  private Mono<ResponseEntity<PurseDto>> update(@PathVariable("id") String id,
                                                @Valid @RequestBody PurseDto walletDto) {
    return ResponseUtil.wrapOrNotFound(purseService.update(id, walletDto));
  }

  @DeleteMapping("/{id}")
  @ResponseStatus(HttpStatus.OK)
  private Mono<ResponseEntity<Void>> delete(@PathVariable("id") String id) {
    return ResponseUtil.wrapOrNotFound(purseService.delete(id));
  }

}
