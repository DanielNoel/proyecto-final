package com.nttdata.microservices.bootcoin.domain.document;

import static com.fasterxml.jackson.annotation.JsonProperty.Access.READ_ONLY;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.time.LocalDateTime;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import lombok.Data;
import lombok.NoArgsConstructor;
/**
 * TIPO DE CAMBIO
 * **/
@Data
@NoArgsConstructor

public class ExchangeRateDto {
  @JsonProperty(access = READ_ONLY)
  private String id;
  private Double purchase; // monto- tasa de cambio
  private Double sale;

}
