package com.nttdata.microservices.bootcoin.domain.service;

import static com.nttdata.microservices.bootcoin.util.MessageUtils.getMsg;

import com.nttdata.microservices.bootcoin.domain.document.PurseDto;
import com.nttdata.microservices.bootcoin.domain.service.mapper.PurseMapper;
import com.nttdata.microservices.bootcoin.exception.BadRequestException;
import com.nttdata.microservices.bootcoin.infraestructure.data.repository.PurseRepository;
import com.nttdata.microservices.bootcoin.domain.interfaces.PurseService;
import java.time.LocalDateTime;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.core.scheduler.Schedulers;

@Slf4j
@Service
@RequiredArgsConstructor
@ConditionalOnProperty(name = "cache.enabled", havingValue = "false", matchIfMissing = true)
public class PurseServiceImpl implements PurseService {

  private final PurseRepository purseRepository;
  protected final PurseMapper purseMapper;

  @Override
  public Flux<PurseDto> findAll() {
    log.debug("list all Wallets");
    return purseRepository.findAll()
        .map(purseMapper::toDto);
  }

  @Override
  public Mono<PurseDto> findById(String id) {
    return purseRepository.findById(id)
        .map(purseMapper::toDto);
  }

  @Override
  public Mono<PurseDto> findByDocumentNumber(String documentNumber) {
    return purseRepository.findByDocumentNumber(documentNumber)
        .map(purseMapper::toDto);
  }

  @Override
  public Mono<PurseDto> create(PurseDto wallet) {
    return Mono.just(wallet)
        .flatMap(this::existWallet)
        .map(purseMapper::toEntity)
        .map(entity -> {
          entity.setRegisterDate(LocalDateTime.now());
          return entity;
        })
        .flatMap(purseRepository::insert)
        .map(purseMapper::toDto)
        .subscribeOn(Schedulers.boundedElastic());
  }

  @Override
  public Mono<PurseDto> update(String id, PurseDto walletDto) {
    return purseRepository.findById(id)
        .flatMap(p -> Mono.just(walletDto)
            .map(purseMapper::toEntity)
            .doOnNext(e -> e.setId(id)))
        .flatMap(this.purseRepository::save)
        .map(purseMapper::toDto);
  }

  @Override
  public Mono<Void> delete(String id) {
    return purseRepository.deleteById(id);
  }

/**
 * validar existencia de Monedero por numero de Documento
**/
  private Mono<PurseDto> existWallet(PurseDto walletDto) {
    log.debug("find Purse by documentNumber: {}", walletDto.getDocumentNumber());

    return findByDocumentNumber(walletDto.getDocumentNumber())
        .flatMap(r -> Mono.error(new BadRequestException(getMsg("purse.already",
            walletDto.getDocumentNumber()))))
        .thenReturn(walletDto);
  }

}
