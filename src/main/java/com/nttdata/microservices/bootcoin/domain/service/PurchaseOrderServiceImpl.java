package com.nttdata.microservices.bootcoin.domain.service;

import static com.nttdata.microservices.bootcoin.util.MessageUtils.getMsg;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.nttdata.microservices.bootcoin.domain.service.mapper.PurseMapper;
import com.nttdata.microservices.bootcoin.infraestructure.data.entity.ExchangeRate;
import com.nttdata.microservices.bootcoin.infraestructure.data.entity.PaymentMethod;
import com.nttdata.microservices.bootcoin.infraestructure.data.entity.PurchaseOrderStatus;
import com.nttdata.microservices.bootcoin.infraestructure.data.entity.TransactionOrder;
import com.nttdata.microservices.bootcoin.infraestructure.data.entity.account.Account;
import com.nttdata.microservices.bootcoin.exception.BadRequestException;
import com.nttdata.microservices.bootcoin.exception.TransactionException;
import com.nttdata.microservices.bootcoin.kafka.KafkaProducerService;
import com.nttdata.microservices.bootcoin.infraestructure.data.repository.ExchangeRateRepository;
import com.nttdata.microservices.bootcoin.infraestructure.data.repository.PurchaseOrderRepository;
import com.nttdata.microservices.bootcoin.infraestructure.data.repository.TransactionOrderRepository;
import com.nttdata.microservices.bootcoin.domain.interfaces.PurchaseOrderService;
import com.nttdata.microservices.bootcoin.domain.interfaces.PurseService;
import com.nttdata.microservices.bootcoin.domain.document.ProcessOrderRequestDto;
import com.nttdata.microservices.bootcoin.domain.document.PurchaseOrderDto;
import com.nttdata.microservices.bootcoin.domain.service.mapper.PurchaseOrderMapper;
import com.nttdata.microservices.bootcoin.util.NumberUtil;
import java.time.LocalDateTime;
import java.util.concurrent.CompletableFuture;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.support.SendResult;
import org.springframework.stereotype.Service;
import org.springframework.util.concurrent.ListenableFutureCallback;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.core.publisher.SignalType;
import reactor.core.publisher.Sinks;

@Slf4j
@RequiredArgsConstructor
@Service
public class PurchaseOrderServiceImpl implements PurchaseOrderService {

  private final PurchaseOrderRepository orderRepository;
  private final ExchangeRateRepository exchangeRateRepository;
  private final TransactionOrderRepository transactionOrderRepository;

  private final PurseService walletService;

  private final PurchaseOrderMapper orderMapper;
  private final PurseMapper purseMapper;

  private final KafkaProducerService<String, String> kafkaProducerService;

  private final ObjectMapper objectMapper;

  private final Sinks.One<PurchaseOrderDto> sseEventSender = Sinks.one();

  @Value("${kafka.topic.account-request}")
  private String topicAccountRequest;

  @Value("${kafka.topic.yanki-request}")
  private String topicYankiRequest;

  @Override
  public Flux<PurchaseOrderDto> findAll() {
    return orderRepository.findAll()
        .map(orderMapper::toDto);
  }

  @Override
  public Mono<PurchaseOrderDto> findById(String id) {
    return orderRepository.findById(id)
        .map(orderMapper::toDto);
  }

  @Override
  public Flux<PurchaseOrderDto> findByDocumentNumberSeller(String documentNumber) {
    return orderRepository.findByWalletSellerDocumentNumber(documentNumber)
        .map(orderMapper::toDto);
  }

  @Override
  public Mono<PurchaseOrderDto> create(PurchaseOrderDto orderDto) {
    return Mono.just(orderDto)
        .flatMap(this::existWalletBuyer)
        .flatMap(this::existWalletSeller)
        .flatMap(this::validatePaymentMethod) //validar si el YANKI O TRANSACCION
        .map(orderMapper::toEntity)
        .map(dto -> {
          dto.setRegisterDate(LocalDateTime.now());
          dto.setStatus(PurchaseOrderStatus.PENDING);
          dto.setOrderCode(NumberUtil.generateRandomNumber(6));
          return dto;
        })
        .flatMap(orderRepository::insert)
        .map(orderMapper::toDto);
  }
  /**
   * validar Monedero del comprador
   * */
  private Mono<PurchaseOrderDto> existWalletBuyer(PurchaseOrderDto purchaseOrderDto) {
    return this.walletService.findByDocumentNumber(purchaseOrderDto.getDocumentNumberBuyer())
        .switchIfEmpty(Mono.error(new BadRequestException(getMsg("purse.buyer.not.found"))))
        .map(purseMapper::toEntity)
        .doOnNext(purchaseOrderDto::setWalletBuyer)
        .thenReturn(purchaseOrderDto);
  }
/**
 * validar Modero del vendedor
 * */
  private Mono<PurchaseOrderDto> existWalletSeller(PurchaseOrderDto purchaseOrderDto) {
    return this.walletService.findByDocumentNumber(purchaseOrderDto.getDocumentNumberSeller())
        .switchIfEmpty(Mono.error(new BadRequestException(getMsg("purse.seller.not.found"))))
        .map(purseMapper::toEntity)
        .doOnNext(purchaseOrderDto::setWalletSeller)
        .thenReturn(purchaseOrderDto);
  }

  /**
   * validar método de pago, si es TRANSFERENCIA se requiere Cuenta Bancaria o sino numero de cel, porque no pueden estar vacio
   * */
  private Mono<PurchaseOrderDto> validatePaymentMethod(PurchaseOrderDto purchaseOrderDto) {
    return Mono.just(purchaseOrderDto)
        .handle((dto, sink) -> {
          if (PaymentMethod.TRANSFER.name().equals(dto.getPaymentMethod())
              && StringUtils.isBlank(dto.getAccountNumber())) {
            sink.error(new BadRequestException("account.number.required"));
          } else if (PaymentMethod.YANKI.name().equals(dto.getPaymentMethod())
              && StringUtils.isBlank(dto.getPhoneNumber())) {
            sink.error(new BadRequestException("yanki.number.required"));
          } else {
            sink.next(dto);
          }
        });
  }

  /**PROCESAR CUANDO LA COMPRA HAS SIDO APROBADO POR EL VENDEDOR   */
  @Override
  public Mono<PurchaseOrderDto> process(ProcessOrderRequestDto processOrderRequestDto) {
    return orderRepository.findByOrderCode(processOrderRequestDto.getOrderCode())
        .map(orderMapper::toDto)
        .flatMap(this::validateStatus)
        .map(orderMapper::toEntity)
        .flatMap(orderRepository::save)
        .map(orderMapper::toDto);
  }

  private Mono<PurchaseOrderDto> updateStatus(PurchaseOrderDto purchaseOrderDto,
                                              PurchaseOrderStatus status) {
    return Mono.just(purchaseOrderDto)
        .map(dto -> {
          dto.setStatus(status);
          dto.setProcessedDate(LocalDateTime.now());
          return dto;
        })
        .map(orderMapper::toEntity)
        .flatMap(orderRepository::save)
        .map(orderMapper::toDto);
  }

/**
 * validar Estado de la Compra, si fue rechazo o aceptado por el vendedor
***/
  private Mono<PurchaseOrderDto> validateStatus(PurchaseOrderDto purchaseOrderDto) {
    return Mono.just(purchaseOrderDto)
        .<PurchaseOrderDto>handle((dto, sink) -> {
          if (PurchaseOrderStatus.REJECTED.equals(purchaseOrderDto.getStatus())) {
            sink.error(new TransactionException(getMsg("transaction.reject.order")));
          } else {
            sink.next(dto);
          }
        })
        .onErrorResume(ex -> this.updateStatus(purchaseOrderDto, PurchaseOrderStatus.REJECTED)
        )
        .flatMap(dto -> {
          if (PaymentMethod.TRANSFER.name().equals(dto.getPaymentMethod())) {
            return validateAccountBuyer(dto);
          } else {
            // validar con MS YANKI
            return Mono.just(dto);
          }
        });
  }
/** validar cuenta del comprador
 * */
  private Mono<PurchaseOrderDto> validateAccountBuyer(PurchaseOrderDto purchaseOrderDto) {

    CompletableFuture<SendResult<String, String>> completable =
        kafkaProducerService.send(topicAccountRequest,
            purchaseOrderDto.getAccountNumber(),
            purchaseOrderDto.getWalletBuyer().getDocumentNumber(),
            new ListenableFutureCallback<>() {
              @Override
              public void onFailure(Throwable ex) {
                Mono.error(new BadRequestException("")).subscribe();
                log.error("Success {}", ex.getMessage(), ex);
              }

              @Override
              public void onSuccess(SendResult<String, String> result) {
                sseEventSender.tryEmitValue(purchaseOrderDto);
                log.debug("Success {}", result);
              }
            }).completable();


    return Mono.fromFuture(completable)
        .doFinally((signalType) -> {
          if (signalType == SignalType.CANCEL) {
            completable.cancel(true);
          }
        })
        .thenReturn(purchaseOrderDto);
  }

  private Mono<PurchaseOrderDto> getExchangeRate(PurchaseOrderDto purchaseOrderDto) {
    return exchangeRateRepository.findTopByOrderByRegisterDateDesc()
        .doOnNext(purchaseOrderDto::setExchangeRate)
        .thenReturn(purchaseOrderDto);
  }
/**
 * CREAR ORDEN DE TRANSACCION
**/
  private Mono<TransactionOrder> buildTransactionOrder(PurchaseOrderDto purchaseOrderDto) {

    Double numberBootCoins = purchaseOrderDto.getAmount();
    ExchangeRate exchange = purchaseOrderDto.getExchangeRate();
    Double purchaseAmount = exchange.getPurchase() * numberBootCoins;
    String paymentMethod = purchaseOrderDto.getPaymentMethod();

    TransactionOrder transactionOrder = new TransactionOrder();
    transactionOrder.setTransactionCode(NumberUtil.generateRandomNumber(8));
    transactionOrder.setAmount(purchaseAmount);
    transactionOrder.setWalletBuyer(purchaseOrderDto.getWalletBuyer());
    transactionOrder.setWalletSeller(purchaseOrderDto.getWalletSeller());
    transactionOrder.setPaymentMethod(PaymentMethod.valueOf(paymentMethod));
    transactionOrder.setExchangeRate(purchaseOrderDto.getExchangeRate());
    transactionOrder.setRegisterDate(LocalDateTime.now());
    return Mono.just(transactionOrder);
  }


}
