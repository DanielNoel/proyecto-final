package com.nttdata.microservices.bootcoin.domain.service.mapper;

import com.nttdata.microservices.bootcoin.domain.service.mapper.base.EntityMapper;
import com.nttdata.microservices.bootcoin.infraestructure.data.entity.ExchangeRate;
import com.nttdata.microservices.bootcoin.domain.document.ExchangeRateDto;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface ExchangeRateMapper extends EntityMapper<ExchangeRateDto, ExchangeRate> {

}
