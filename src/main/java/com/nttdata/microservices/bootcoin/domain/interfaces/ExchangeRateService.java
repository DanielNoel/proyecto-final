package com.nttdata.microservices.bootcoin.domain.interfaces;

import com.nttdata.microservices.bootcoin.domain.document.ExchangeRateDto;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
/**
 *Tipo de cambio
 * */
public interface ExchangeRateService {

  Flux<ExchangeRateDto> findAll();

  Mono<ExchangeRateDto> findById(String id);

  Mono<ExchangeRateDto> create(ExchangeRateDto wallet);

}
