package com.nttdata.microservices.bootcoin.domain.service.mapper;

import com.nttdata.microservices.bootcoin.domain.service.mapper.base.EntityMapper;
import com.nttdata.microservices.bootcoin.infraestructure.data.entity.PurchaseOrder;
import com.nttdata.microservices.bootcoin.domain.document.PurchaseOrderDto;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface PurchaseOrderMapper extends EntityMapper<PurchaseOrderDto, PurchaseOrder> {
}
