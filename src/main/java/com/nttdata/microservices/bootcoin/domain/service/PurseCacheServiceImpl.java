package com.nttdata.microservices.bootcoin.domain.service;

import com.nttdata.microservices.bootcoin.domain.document.PurseDto;
import com.nttdata.microservices.bootcoin.domain.service.mapper.PurseMapper;
import com.nttdata.microservices.bootcoin.infraestructure.data.repository.PurseRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.data.redis.core.ReactiveHashOperations;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Slf4j
@Service
@ConditionalOnProperty(name = "cache.enabled", havingValue = "true")
public class PurseCacheServiceImpl extends PurseServiceImpl {

  private final ReactiveHashOperations<String, String, PurseDto> redisOperations;

  private static final String KEY_CACHE = "wallets";

  public PurseCacheServiceImpl(PurseRepository purseRepository, PurseMapper purseMapper,
                               ReactiveHashOperations<String, String, PurseDto> redisOperations) {
    super(purseRepository, purseMapper);
    this.redisOperations = redisOperations;
  }

  @Override
  public Flux<PurseDto> findAll() {
    return redisOperations.values(KEY_CACHE)
        .switchIfEmpty(getAllFromDatabase());
  }

  @Override
  public Mono<PurseDto> findById(String id) {
    return redisOperations.get(KEY_CACHE, id)
        .switchIfEmpty(this.getFromDatabase(id));
  }

  @Override
  public Mono<PurseDto> findByDocumentNumber(String documentNumber) {
    return redisOperations.values(KEY_CACHE)
        .filter(walletDto -> walletDto.getDocumentNumber().equals(documentNumber))
        .singleOrEmpty()
        .switchIfEmpty(super.findByDocumentNumber(documentNumber));
  }

//  @Override
//  public Mono<Void> findByDocumentNumberForKafka(String documentNumber) {
//    log.debug("kafka client find by documentNumber: {}", documentNumber);
//    return this.findByDocumentNumber(documentNumber)
//        .map(purseMapper::toEntity)
//        .doOnNext(dto -> {
//          log.debug("kafka client find result: {}", dto);
//          kafkaProducerService.send(topic, dto);
//        })
//        .then();
//  }

  @Override
  public Mono<PurseDto> create(PurseDto wallet) {
    return super.create(wallet)
        .flatMap(this::saveCacheRedis);
  }

  @Override
  public Mono<PurseDto> update(String id, PurseDto walletDto) {
    return this.redisOperations.remove(KEY_CACHE, id)
        .then(super.update(id, walletDto))
        .flatMap(this::saveCacheRedis);
  }

  private Mono<PurseDto> saveCacheRedis(PurseDto walletDto) {
    log.info("put redis cache PurseDto: {}", walletDto);
    return this.redisOperations.put(KEY_CACHE,
            walletDto.getId(),
            walletDto)
        .thenReturn(walletDto);
  }

  private Mono<PurseDto> getFromDatabase(String id) {
    return super.findById(id)
        .flatMap(dto -> this.redisOperations.put(KEY_CACHE, id, dto)
            .thenReturn(dto));
  }

  private Flux<PurseDto> getAllFromDatabase() {
    return super.findAll()
        .flatMap(dto -> this.redisOperations.put(KEY_CACHE, dto.getId(), dto)
            .thenReturn(dto));
  }


}
