package com.nttdata.microservices.bootcoin.domain.service.mapper;

import com.nttdata.microservices.bootcoin.infraestructure.data.entity.Purse;
import com.nttdata.microservices.bootcoin.domain.document.PurseDto;
import com.nttdata.microservices.bootcoin.domain.service.mapper.base.EntityMapper;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface PurseMapper extends EntityMapper<PurseDto, Purse> {

}
