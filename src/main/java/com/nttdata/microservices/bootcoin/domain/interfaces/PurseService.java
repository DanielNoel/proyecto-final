package com.nttdata.microservices.bootcoin.domain.interfaces;

import com.nttdata.microservices.bootcoin.domain.document.PurseDto;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface PurseService {

  Flux<PurseDto> findAll();

  Mono<PurseDto> findById(String id);

  Mono<PurseDto> findByDocumentNumber(String documentNumber);

  Mono<PurseDto> create(PurseDto wallet);

  Mono<PurseDto> update(String id, PurseDto walletDto);

  Mono<Void> delete(String id);
}
