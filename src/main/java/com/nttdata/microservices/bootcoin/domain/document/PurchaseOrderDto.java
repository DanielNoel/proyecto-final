package com.nttdata.microservices.bootcoin.domain.document;

import static com.fasterxml.jackson.annotation.JsonProperty.Access.READ_ONLY;
import static com.fasterxml.jackson.annotation.JsonProperty.Access.WRITE_ONLY;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.nttdata.microservices.bootcoin.infraestructure.data.entity.ExchangeRate;
import com.nttdata.microservices.bootcoin.infraestructure.data.entity.PaymentMethod;
import com.nttdata.microservices.bootcoin.infraestructure.data.entity.PurchaseOrderStatus;
import com.nttdata.microservices.bootcoin.infraestructure.data.entity.Purse;
import com.nttdata.microservices.bootcoin.util.validation.ValueOfEnum;
import java.time.LocalDateTime;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 *
 * Orden de compra
 ***/
@Data
@NoArgsConstructor
public class PurchaseOrderDto {
  @JsonProperty(access = READ_ONLY)
  private String id;

  @JsonProperty(access = WRITE_ONLY)
  @NotBlank(message = "documentNumberBuyer is required")

  private String documentNumberBuyer;

  @JsonProperty(access = WRITE_ONLY)
  @NotBlank(message = "documentNumberSeller is required")
  private String documentNumberSeller;

  @NotNull(message = "amount is required")
  @Positive(message = "amount must be greater than zero (0)")
  private Double amount;

  @NotNull(message = "paymentMethod is required")
  @ValueOfEnum(enumClass = PaymentMethod.class, message = "paymentMethod is invalid value")
  private String paymentMethod;

  private String accountNumber;

  private String phoneNumber;

  @JsonProperty(access = READ_ONLY)
  private String orderCode;

  @JsonProperty(access = READ_ONLY)
  private PurchaseOrderStatus status;

  @JsonProperty(access = READ_ONLY)
  private LocalDateTime registerDate;

  @JsonProperty(access = READ_ONLY)
  private LocalDateTime processedDate;



  @JsonProperty(access = READ_ONLY)
  private Purse walletBuyer;

  @JsonProperty(access = READ_ONLY)
  private Purse walletSeller;

  @JsonProperty(access = READ_ONLY)
  private ExchangeRate exchangeRate;

}
