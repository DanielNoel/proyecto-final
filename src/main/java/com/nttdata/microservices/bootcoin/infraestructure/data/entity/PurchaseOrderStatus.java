package com.nttdata.microservices.bootcoin.infraestructure.data.entity;
/**
 * PENDIENTE, ACEPTADO O RECHAZADO
 * */
public enum PurchaseOrderStatus {
  PENDING, ACCEPTED, REJECTED;
}
