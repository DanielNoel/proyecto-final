package com.nttdata.microservices.bootcoin.infraestructure.data.repository;

import com.nttdata.microservices.bootcoin.infraestructure.data.entity.ExchangeRate;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Mono;

@Repository
public interface ExchangeRateRepository extends ReactiveMongoRepository<ExchangeRate, String> {

  Mono<ExchangeRate> findTopByOrderByRegisterDateDesc();
}
