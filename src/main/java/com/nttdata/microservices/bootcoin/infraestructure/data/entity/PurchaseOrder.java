package com.nttdata.microservices.bootcoin.infraestructure.data.entity;

import java.time.LocalDateTime;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.DocumentReference;
/**
 * orden de compra
 * **/
@Data
@EqualsAndHashCode(callSuper = false)
@NoArgsConstructor
@Document("purchase-order")
public class PurchaseOrder  {

  @Id
  private String id;
  private String orderCode;
  @DocumentReference
  private Purse walletBuyer;
  @DocumentReference
  private Purse walletSeller;
  private Double amount;
  private PaymentMethod paymentMethod;
  private String accountNumber;
  private String phoneNumber;
  private PurchaseOrderStatus status;
  private LocalDateTime registerDate;
  private LocalDateTime processedDate;

}
