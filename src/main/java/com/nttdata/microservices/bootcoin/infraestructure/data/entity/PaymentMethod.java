package com.nttdata.microservices.bootcoin.infraestructure.data.entity;

public enum PaymentMethod {
  YANKI, TRANSFER
}
