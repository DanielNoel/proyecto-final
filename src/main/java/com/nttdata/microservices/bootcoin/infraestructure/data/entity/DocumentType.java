package com.nttdata.microservices.bootcoin.infraestructure.data.entity;

public enum DocumentType {
  DNI, CEX, PASSPORT;
}
