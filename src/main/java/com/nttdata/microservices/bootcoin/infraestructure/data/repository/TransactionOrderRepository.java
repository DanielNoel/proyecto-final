package com.nttdata.microservices.bootcoin.infraestructure.data.repository;

import com.nttdata.microservices.bootcoin.infraestructure.data.entity.TransactionOrder;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TransactionOrderRepository
    extends ReactiveMongoRepository<TransactionOrder, String> {
}
