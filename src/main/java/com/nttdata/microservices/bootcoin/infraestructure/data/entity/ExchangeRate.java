package com.nttdata.microservices.bootcoin.infraestructure.data.entity;

import java.time.LocalDateTime;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
/***
 *
 tipo de cambio
 * */
@Data
@EqualsAndHashCode(callSuper = false)
@NoArgsConstructor
@Document(collection = "exchange-rate")
public class ExchangeRate  {

  @Id
  private String id;
  private Double purchase;
  private Double sale;
  private LocalDateTime registerDate;

}
