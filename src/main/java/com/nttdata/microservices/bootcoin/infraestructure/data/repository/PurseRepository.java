package com.nttdata.microservices.bootcoin.infraestructure.data.repository;

import com.nttdata.microservices.bootcoin.infraestructure.data.entity.Purse;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Mono;

@Repository
public interface PurseRepository extends ReactiveMongoRepository<Purse, String> {

  Mono<Purse> findByDocumentNumber(String documentNumber);

}
