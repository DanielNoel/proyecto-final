package com.nttdata.microservices.bootcoin.exception;

public class PurseException extends RuntimeException {

  private String message;
  private Integer statusCode;

  public PurseException(String message, Integer statusCode) {
    super(message);
    this.message = message;
    this.statusCode = statusCode;
  }

  public PurseException(String message) {
    super(message);
  }

  public PurseException(String message, Throwable cause) {
    super(message, cause);
  }
}